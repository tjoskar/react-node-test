const history = require('connect-history-api-fallback')
const express = require('express')
const { spawn } = require('child_process')

const install = new Promise((resolve, reject) => {
  const webpackBuild = spawn(__dirname + '/node_modules/.bin/webpack', [
    '-c',
    'webpack.config.js',
    '-p'
  ])

  webpackBuild.stdout.on('data', data => console.log(String(data)))
  webpackBuild.stderr.on('error', error => console.error(error))
  webpackBuild.on('close', code => {
    if (code === 0) {
      resolve(code)
    } else {
      reject(new Error('Webpack exit with status code' + code))
    }
  })
})

install
  .then(() => {
    console.log('Webpack is complete!')
  })
  .catch(error => {
    console.log(error)
    process.exit(1)
  })

const app = express()
app.use(
  history({
    verbose: true
  })
)
app.use(express.static('./dist'))
app.listen(process.env.PORT || 3000, function() {
  console.log('Example app listening on port 3000!')
})
